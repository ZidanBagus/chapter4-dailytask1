/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat HTTP Server
 */
const http = require('http');


// import atau panggil dotenv module (third party module)
require('dotenv').config();
const PORT = process.env.PORT;
// const { PORT = 8000 } = process.env; // Ambil port dari environment variable
// const PORT = 8000;

// Request handler
// Fungsi yang berjalan ketika ada request yang masuk.
function onRequest(req, res) {
  // Memberi status 200
  res.writeHead(200);
  res.end("Halo dari server!");
}

const fs = require('fs');
const path = require('path');
let rawData = fs.readFileSync("datane.json");
//assign data dalam bentuk array object
const datane = require("./datane.js");
//assign data yang telah diparsing dari json
const parsing = JSON.parse(rawData);
//destructuring object dari sortData.js
const { data1, data2, data3, data4, data5} = require("./sortData.js");
const data = JSON.stringify(datane);
const PUBLIC_DIRECTORY = path.join(__dirname, 'browser');

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8');
}

function onRequest(req, res) {
  switch (req.url) {
    case "/":
      res.writeHead(200);
      res.end(getHTML("index.html"));
      return;
   case "/data1":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data1(parsing));
      return;
    case "/data2":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data2(parsing));
      return;
    case "/data3":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data3(parsing));
      return;
    case "/data4":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data4(parsing));
      return;
    case "/data5":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data5(parsing));
      return;
    case "/about":
      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(getHTML("about.html"));
      return;
  
    default:
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, '0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})
